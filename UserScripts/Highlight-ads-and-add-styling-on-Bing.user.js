// ==UserScript==
// @name         Highlight ads and add styling on Bing.com
// @namespace    https://www.bing.com/
// @version      0.1
// @description  An Userscript for Tampermonkey to highlight ads on Bing.com and differentiate them from organic results.
// @author       Patrick Smits
// @match        https://www.bing.com/*
// @icon         https://t0.gstatic.com/faviconV2?client=SOCIAL&type=FAVICON&fallback_opts=TYPE,SIZE,URL&url=https://www.bing.com/sa/simg/favicon-white-bg-shdw-gra-mg.ico&size=64
// @grant        GM_addStyle
// @run-at       document-start
// ==/UserScript==

let hyperlinkGoogleBlue = '#1a0dab'; // Google Search link color not visited
let hyperlinkGoogleVisited = '#681da8'; // visited Google link

let hyperlinkColor = 'color: ' + hyperlinkGoogleBlue;
console.log('Hyperlink color is ' + hyperlinkColor);

GM_addStyle ( `
    .sb_add {
        background-color:#FFFFEA !important;
    }
` );

function addGlobalStyle(css) {
    var head, style;
    head = document.getElementsByTagName('head')[0];
    if (!head) { return; }
    style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = css.replace(/;/g, ' !important;');
    head.appendChild(style);
}

addGlobalStyle(`h2 { font-family: "arial"; font-size: 20px; line-height:26px; font-weight: 400; }`);
addGlobalStyle(`h2 a:hover { ${hyperlinkGoogleBlue}; font-weight: 600; }`);
addGlobalStyle(`h2 a:visited {${hyperlinkGoogleVisited}; font-weight:600; }`);
