// ==UserScript==
// @name         Highlight Nested Divs
// @namespace    http://tampermonkey.net/
// @version      1.0
// @description  Highlights deeply nested div elements to identify excessive nesting
// @author       You
// @match        *://*/*
// @grant        GM_addStyle
// ==/UserScript==

(function() {
    'use strict';

    // CSS styles for highlighting nested divs
    GM_addStyle(`
        div > div:only-child { outline: 5px solid green; }
        div > div > div:only-child { outline: 5px solid yellow; }
        div > div > div > div:only-child { outline: 5px solid orange; }
        div > div > div > div > div:only-child { outline: 5px solid red; }
    `);
})();